<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="keywords" content="score, film, video, games, voiceover, design, sound, web, movie, music, original, composition, independent" />
<meta name="author" content="Michael R. Myers">
<link rel="icon" href="/favicon.ico">