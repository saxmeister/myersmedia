<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="keywords" content="score, film, video, games, voiceover, design, sound, web, movie, music, original, composition, independent" />
<meta name="description" content="Samples of some of the original music and arrangements that Michael has produced." />
<meta name="author" content="Michael R. Myers">

<link rel="icon" href="favicon.ico">

<title>Music | myersmedia - Saxophonist, composer, arranger, voiceover artist, and sound designer Michael R. Myers</title>

<?php include 'inc/css.inc' ?>

<style type="text/css" media="screen">
.imgbox a {
	display:block;
	text-align:center;
}

.imgbox img {
	height:75px;
	margin:20px auto !important;
}
</style>
</head>

<body class="music">

	<a href="#content" class="sr-only sr-only-focusable">Skip to main content</a>

	<nav class="navbar navbar-expand-lg navbar-inverse navbar-fixed-top bg-faded">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<a class="navbar-brand" href="/"><span>myers</span>media</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="nav-item"><a class="nav-link" href="/">Home</a></li>
					<li class="nav-item"><a class="nav-link" href="about">About</a></li>
					<li class="nav-item active"><a class="nav-link" href="music">Music</a></li>
					<li class="nav-item"><a class="nav-link" href="voiceover">Voiceover</a></li>
					<li class="nav-item"><a class="nav-link" href="sounddesign">Sound Design</a></li>
					<li class="nav-item"><a class="nav-link" href="fees">Arranging &amp; Copy</a></li>
					<li class="nav-item"><a class="nav-link" href="projects">Projects</a></li>
					<li class="nav-item"><a class="nav-link" href="studio">Studio</a></li>
					<li class="nav-item"><a class="nav-link" href="contact">Contact</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>


	<!-- Main jumbotron -->
	<div class="jumbotron" style="background-image:url('img/jumbotron/score_blur.jpg');">
		<div class="container">
			<div class="col-md-12">
				<h1>music</h1>
				<h2>Below are some examples of Michael's musical projects</h2>
			</div>
		</div><!-- /container -->
	</div><!--/jumbotron-->

	<div class="container spaced">
		<div class="row">
			<div class="col-md-12">
				<h2>Arrangements</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<p>Michael has arranged over 100 different pieces for different ensembles with a focus on smaller chamber groups, especially the saxophone quartet. Many of his arrangements may be purchased directly or online.</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<iframe width="100%" height="300" src="https://www.youtube.com/embed/gW338sXijW4" frameborder="0" allowfullscreen></iframe>
				<h3>Christmas Time Is Here</h3>
				<p>An arrangement of the Vince Guaraldi Christmas Classic performed by Lenoir Sax with Michael on the baritone saxophone.</p>
				<p>This arrangement can be purchased at <a href="http://www.sheetmusicplus.com/title/christmas-time-is-here-digital-sheet-music/20423605">SheetMusicPlus.com</a></p>

			</div>
			
			<div class="col-md-6">
				<iframe width="100%" height="300" src="https://www.youtube.com/embed/SFl6qmnHzhg" frameborder="0" allowfullscreen></iframe>
				<h3>Riu, Riu, Chiu</h3>
				<p>An old Spanish Carol arranged for and performed by Lenoir Sax with Michael on the baritone saxophone.</p>

			</div>
		</div><!--/row-->
	</div>

	<div class="container spaced">
		<div class="row">
			<div class="col-md-12">
				<h2>Game Music</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<iframe width="100%" height="300" src="https://www.youtube.com/embed/I0x-atnQOTU" frameborder="0" allowfullscreen></iframe>
				<h3>The DarkMOD: Caves</h3>
				<p>Ambient track &quot;Caves&quot; written for The DarkMOD project.</p>
			</div>

			<div class="col-md-6">
				<iframe width="100%" height="300" src="https://www.youtube.com/embed/hHUTbDSf_as" frameborder="0" allowfullscreen></iframe>
				<h3>The DarkMOD</h3>
				<p>A few selections from The DarkMOD including a simulated Victrola recording written my Michael.</p>
			</div>
		</div>
	</div>

	<div class="container spaced">
		<div class="row">
			<div class="col-md-12">
				<h2>Commercial Work</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/52567775&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>			</div>
			<div class="col-md-6">
				<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/329212126&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
			</div>
		</div>
	</div>


	<div class="container spaced">
		<div class="row">
			<div class="col-md-12">
				<h2>Covers</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<iframe width="100%" height="300" src="https://www.youtube.com/embed/AMmE_nP2hpI" frameborder="0" allowfullscreen></iframe>
				<h3>The Walking Dead Theme (WIP)</h3>
				<p>A sample from a cover of The Walking Dead theme before percussive elements had been added.</p>
			</div>
		</div>
	</div>

	<div class="container">
		<h2>Schl&ouml;ckwerk</h2>
		<p>Michael leads a double-life with his alter ego, Schl&ouml;ckwerk. Lovingly hand-crafted pastiches of things from his past make their appearances with this side project. Expect the unexpected!</p>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/320039273&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
			</div>
			<div class="col-md-6">
				<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/319938908&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
			</div>
			<div class="col-md-6">
				<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/318621302&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
			</div>
			<div class="col-md-6">
				<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/317781902&amp;color=ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
			</div>
			<div class="col-md-6">
				<iframe width="100%" height="166" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/370012082&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
			</div>
			<div class="col-md-6">
				<iframe width="100%" height="160" scrolling="no" frameborder="no" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/318899677&amp;color=%23ff5500&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false"></iframe>
			</div>
		</div>
	</div>



	<footer id="footer" class="clearfix">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 col-sm-6">
					<div id="copyright">Copyright &copy; 2000-<?php echo date("Y"); ?> myersmedia</div>
					<p>&ldquo;He who has ears, let him hear&rdquo; - <em>Matthew 11:15</em></p>
				</div>
				<div class="col-lg-6 col-sm-6 text-right">
					<ul class="footer-links">
						<li><a href="/">Home</a></li>
						<li><a href="about">About</a></li>
						<li><a href="music" class="active">Music</a></li>
						<li><a href="voiceover">Voiceover</a></li>
						<li><a href="sounddesign">Sound Design</a></li>
						<li><a href="fees">Arranging &amp; Copy</a></li>
						<li><a href="projects">Projects</a></li>
						<li><a href="studio">Studio</a></li>
						<li><a href="contact">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

<?php include 'inc/js.inc' ?>

</body>
</html>