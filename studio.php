<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="keywords" content="score, film, video, games, voiceover, design, sound, web, movie, music, original, composition, independent" />
<meta name="description" content="A brief overview of the secret weapons in Michael's little studio." />
<meta name="author" content="Michael R. Myers">

<link rel="icon" href="favicon.ico">

<title>Studio | myersmedia - Saxophonist, composer, arranger, voiceover artist, and sound designer Michael R. Myers</title>

<?php include 'inc/css.inc' ?>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

</head>

<body class="studio">

	<a href="#content" class="sr-only sr-only-focusable">Skip to main content</a>

	<nav class="navbar navbar-expand-lg navbar-inverse navbar-fixed-top bg-faded">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<a class="navbar-brand" href="/"><span>myers</span>media</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="nav-item"><a class="nav-link" href="/">Home</a></li>
					<li class="nav-item"><a class="nav-link" href="about">About</a></li>
					<li class="nav-item"><a class="nav-link" href="music">Music</a></li>
					<li class="nav-item"><a class="nav-link" href="voiceover">Voiceover</a></li>
					<li class="nav-item"><a class="nav-link" href="sounddesign">Sound Design</a></li>
					<li class="nav-item"><a class="nav-link" href="fees">Arranging &amp; Copy</a></li>
					<li class="nav-item"><a class="nav-link" href="projects">Projects</a></li>
					<li class="nav-item active"><a class="nav-link" href="studio">Studio</a></li>
					<li class="nav-item"><a class="nav-link" href="contact">Contact</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>


	<!-- Main jumbotron -->
	<div class="jumbotron" style="background-image:url('img/jumbotron/studio_blur.jpg')">
		<div class="container">
			<div class="col-md-12">
				<h1>studio</h1>
				<h2>Details of Michael's project studio</h2>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<p>The project studio is equipped to handle just about any project and has been the base for composition, arranging, and recording smaller groups and voiceover. Any larger ensembles or location recording can be scheduled for any need.</p>
				<p>We can also partner with various studios and locations in the area to make scheduling a recording as simple as possible.</p>
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-lg-3">
				<h3>Main System</h3>

				<ul class="pl-3">
					<li>Custom-built Intel quad-core Windows/Linux machine</li>
					<li>Windows 10 (64-bit)</li>
					<li>Cakewalk SONAR Producer X3</li>
				</ul>

				<h3>Mobile Systems</h3>
				<ul class="pl-3">
					<li>Windows 7 Pro or Windows 10</li>
					<li>Cakewalk SONAR Producer</li>
					<li>Finale</li>
				</ul>

				<h3>Interfaces</h3>
				<ul class="pl-3">
					<li>Focusrite Scarlett Series</li>
					<li>Focusrite Saffire Series</li>
				</ul>

				<h3>Scoring Software</h3>
				<ul class="pl-3">
					<li>Finale</li>
				</ul>

			</div>

			<div class="col-lg-3">

				<h3>Microphones</h3>
				<ul class="pl-3">
					<li>Shure</li>
					<li>BLUE</li>
					<li>Audio-Technica</li>
					<li>Avantone</li>
					<li>Sennheiser</li>
					<li>R&oslash;de</li>
					<li>CAD</li>
					<li>MXL</li>
					<li>Electro-Voice</li>
				</ul>

				<h3>Outboard Gear</h3>
				<ul class="pl-3">
					<li>dbx</li>
					<li>Lexicon</li>
					<li>Symetrix</li>
					<li>ART</li>
					<li>M-Audio</li>
					<li>Behringer</li>
				</ul>

			</div>

			<div class="col-lg-3">

				<h3>Keys &amp; Controllers</h3>
				<ul class="pl-3">
					<li>Kawai 88-key weighted action</li>
					<li>Edirol 64-key synth action</li>
					<li>Arturia MINILAB</li>
					<li>Akai MPX percussion pad</li>
					<li>Various other controllers</li>
				</ul>
				<h3>Synths</h3>
				<ul class="pl-3">
					<li>E-MU 5000 Ultra</li>
					<li>E-MU Proteus/1</li>
					<li>Korg TR-Rack</li>
					<li>Korg Monotron Delay</li>
					<li>Roland MKS-20</li>
					<li>Roland MT-32</li>
					<li>Custom built synths</li>
				</ul>

				<h3>Virtual Gear</h3>
				<ul class="pl-3">
					<li>Cakewalk</li>
					<li>Waves</li>
					<li>SSL</li>
					<li>Nomad Factory</li>
					<li>Blue Cat</li>
					<li>Focusrite</li>
					<li>Overloud</li>
					<li>AmpliTube</li>
				</ul>
				
			</div>

			<div class="col-lg-3">

				<h3>Virtual Instruments</h3>
				<ul class="pl-3">
					<li>Cakewalk Rapture</li>
					<li>Dimension Pro</li>
					<li>Session Drummer</li>
					<li>Cakewalk Sound Center</li>
					<li>Arturia Analog Lab, SEM</li>
					<li>Garritan Personal Orchestra</li>
					<li>IK Multimedia CS-80V, SampleTron, SampleMoog</li>
					<li>Miroslav Philharmonik</li>
					<li>Native Instruments Kontakt, Massive</li>
					<li>Korg Legacy Cell, MS-20, M1, Polysix, Wavestation, Mono/Poly</li>
				</ul>

			</div>


		</div>
	</div>





	<footer id="footer" class="clearfix">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 col-sm-6">
					<div id="copyright">Copyright &copy; 2000-<?php echo date("Y"); ?> myersmedia</div>
					<p>&ldquo;He who has ears, let him hear&rdquo; - <em>Matthew 11:15</em></p>
				</div>
				<div class="col-lg-6 col-sm-6 text-right">
					<ul class="footer-links">
						<li><a href="/">Home</a></li>
						<li><a href="about">About</a></li>
						<li><a href="music">Music</a></li>
						<li><a href="voiceover">Voiceover</a></li>
						<li><a href="sounddesign">Sound Design</a></li>
						<li><a href="fees">Arranging &amp; Copy</a></li>
						<li><a href="projects">Projects</a></li>
						<li><a href="studio" class="active">Studio</a></li>
						<li><a href="contact">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

<?php include 'inc/js.inc' ?>

</body>
</html>