<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="keywords" content="portfolio" />
<meta name="description" content="Portfolio of web designer and developer Michael R. Myers." />
<meta name="author" content="Michael R. Myers">

<link rel="icon" href="favicon.ico">

<title>Portfolio | myersmedia - Saxophonist, composer, arranger, voiceover artist, and sound designer Michael R. Myers</title>

<?php include '../inc/css.inc' ?>

<link rel="stylesheet" media="screen" href="../css/jumbotron.css">

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/"><span>myers</span>media</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="/">Home</a></li>
					<li><a href="/about">About</a></li>
					<li><a href="/music">Music</a></li>
					<li><a href="/voiceover">Voiceover</a></li>
					<li><a href="/sounddesign">Sound Design</a></li>
					<li><a href="/projects">Projects</a></li>
					<li><a href="/studio">Studio</a></li>
					<li><a href="/contact">Contact</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>



	<!-- Main jumbotron -->
	<div class="jumbotron">
		<div class="container-fluid" style="width:100%;height:100%;background-color:rgba(0,0,0,0.5);">
			<div class="container">
				<h1>Portfolio</h1>
				<p>Below are some of the highlights of the work of Michael R. Myers as a web designer and developer</p>
				<p>Follow the links in the thumbnails below to view sites that Michael has worked on over the course of his career.</p>
			</div>
		</div>
	</div>



	<!-- Three columns of text below the carousel -->
	<div class="container marketing">
		<div class="row">

			<div class="col-lg-6" style="text-align:left;margin-bottom:20px">
				<img src="img/cdo.jpg" alt="" style="max-width:100%;margin:0 auto 10px auto;box-shadow:0 0 5px rgba(0,0,0,0.5);" />
				<h2>Climate Data Online</h2>
				<p>Michael designed the UI and overall look and feel of this site for the client to allow a simpler and faster access point for data.</p>
				<p><a href="https://www.ncdc.noaa.gov/cdo-web" class="btn btn-default" role="button">Visit &raquo;</a></p>
			</div><!-- /.col-lg-6 -->

			<div class="col-lg-6" style="text-align:left;margin-bottom:20px">
				<img src="img/ncdc.jpg" alt="" style="max-width:100%;margin:0 auto 10px auto;box-shadow:0 0 5px rgba(0,0,0,0.5);" />
				<h2>NCDC</h2>
				<p>A site that was over 12 years old and desperately needed a facelift got what it needed with the guidance of Michael.</p>
				<p><a href="https://www.ncdc.noaa.gov" class="btn btn-default" role="button">Visit &raquo;</a></p>
			</div><!-- /.col-lg-6 -->

			<div class="col-lg-6" style="text-align:left;margin-bottom:20px">
				<img src="img/ncei.jpg" alt="" style="max-width:100%;margin:0 auto 10px auto;box-shadow:0 0 5px rgba(0,0,0,0.5);" />
				<h2>NCEI</h2>
				<p>When NCDC reorganized and rebranded itself, Michael was there to assist and guide the new look and feel of the organization's web presence.</p>
				<p><a href="https://www.ncei.noaa.gov" class="btn btn-default" role="button">Visit &raquo;</a></p>
			</div><!-- /.col-lg-6 -->

			<div class="col-lg-6" style="text-align:left;margin-bottom:20px">
				<img src="img/climate.jpg" alt="" style="max-width:100%;margin:0 auto 10px auto;box-shadow:0 0 5px rgba(0,0,0,0.5);" />
				<h2>Climate.gov</h2>
				<p>As a developer, Michael has assisted this site by developing custom modules and interactive maps, as well as day-to-day site maintenance and expansion.</p>
				<p><a href="https://www.climate.gov" class="btn btn-default" role="button">Visit &raquo;</a></p>
			</div><!-- /.col-lg-6 -->

			<div class="col-lg-6" style="text-align:left;margin-bottom:20px">
				<img src="img/lenoirsax.jpg" alt="" style="max-width:100%;margin:0 auto 10px auto;box-shadow:0 0 5px rgba(0,0,0,0.5);" />
				<h2>Lenoir Sax</h2>
				<p>As a member of this professional chamber music group, Michael developed this custom responsive site featuring custom media handling and performance calendars.</p>
				<p><a href="http://www.lenoirsax.org" class="btn btn-default" role="button">Visit &raquo;</a></p>
			</div><!-- /.col-lg-6 -->

			<div class="col-lg-6" style="text-align:left;margin-bottom:20px">
				<div style="display:block;max-width:100%;height:200px;margin:0 auto 10px auto;box-shadow:0 0 5px rgba(0,0,0,0.5);max-height:321px;height:321px;background-color:#f0f0f8"></div>
                <h2>Other Notable Work</h2>
                <ul>
                    <li><a href="http://www.citizen-times.com">Citizen-Times</a> as designer/developer</li>
                    <li><a href="http://www.iwanna.com/">IWANNA</a> as designer/developer</li>
                    <li><a href="http://www.cannondale.com/en/USA">Cannondale Bicycles</a> as web manager</li>
                    <li><a href="http://www.mongoose.com/usa/">Mongoose Bicycles</a> as web manager</li>
                    <li><a href="http://www.schwinnbikes.com/usa/">Schwinn Bicycles</a> as web manager</li>
                    <li><a href="https://www.dorel-ecommerce.com">Dorel</a> as web manager and CMS UI developer</li>
                </ul>
			</div><!-- /.col-lg-6 -->

		</div><!-- /.row -->
	</div>

	<div class="container">

	</div><!-- /container -->

<footer id="footer" class="clearfix">

	<div class="col-lg-6 align-left">
		<div id="copyright">&copy; Copyright 2000-<?php echo date("Y"); ?> myersmedia</div>
		<p>&ldquo;He who has ears, let him hear&rdquo; - <em>Matthew 11:15</em></p>
	</div>
	<div class="col-lg-6 align-right">
		<ul class="footer-links">
			<li><a href="/" class="active">Home</a></li>
			<li><a href="/about">About</a></li>
			<li><a href="/music">Music</a></li>
			<li><a href="/voiceover">Voiceover</a></li>
			<li><a href="/sounddesign">Sound Design</a></li>
			<li><a href="/projects">Projects</a></li>
			<li><a href="/studio">Studio</a></li>
			<li><a href="/contact">Contact</a></li>
		</ul>
	</div>

</footer>

<?php include '../inc/js.inc' ?>

</body>
</html>