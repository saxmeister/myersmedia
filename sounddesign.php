<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="keywords" content="score, film, video, games, voiceover, design, sound, web, movie, music, original, composition, independent" />
<meta name="description" content="Samples of Michael's work as a sound designer." />
<meta name="author" content="Michael R. Myers">

<link rel="icon" href="favicon.ico">

<title>Sound Design | myersmedia - Saxophonist, composer, arranger, sound designer and voiceover artist Michael R. Myers</title>

<?php include 'inc/css.inc' ?>

</head>

<body class="sounddesign">

	<a href="#content" class="sr-only sr-only-focusable">Skip to main content</a>

	<nav class="navbar navbar-expand-lg navbar-inverse navbar-fixed-top bg-faded">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<a class="navbar-brand" href="/"><span>myers</span>media</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="nav-item"><a class="nav-link" href="/">Home</a></li>
					<li class="nav-item"><a class="nav-link" href="about">About</a></li>
					<li class="nav-item"><a class="nav-link" href="music">Music</a></li>
					<li class="nav-item"><a class="nav-link" href="voiceover">Voiceover</a></li>
					<li class="nav-item active"><a class="nav-link" href="sounddesign">Sound Design</a></li>
					<li class="nav-item"><a class="nav-link" href="fees">Arranging &amp; Copy</a></li>
					<li class="nav-item"><a class="nav-link" href="projects">Projects</a></li>
					<li class="nav-item"><a class="nav-link" href="studio">Studio</a></li>
					<li class="nav-item"><a class="nav-link" href="contact">Contact</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>


	<!-- Main jumbotron -->
	<div class="jumbotron" style="background-image:url('img/jumbotron/mixer_blur.jpg')">
		<div class="container">
			<div class="col-md-12">
				<h1>sound design</h1>
				<h2>Below are some examples of Michael's sound design work</h2>
			</div>
		</div>
	</div>

	<div class="container">
		<h2>Games</h2>
	</div>

	<div class="container">
		<div class="row">

			<div class="col-md-6">
				<iframe width="100%" height="300" src="https://www.youtube.com/embed/hHUTbDSf_as" frameborder="0" allowfullscreen></iframe>
				<h3>The DarkMOD</h3>
				<p>Composition and sound design for The DarkMOD. The first selection is my composition and the second and third are from another team member.</p>
			</div>

			<div class="col-md-6">
				<iframe width="100%" height="300" src="https://www.youtube.com/embed/j0B6hnjfgIw" frameborder="0" allowfullscreen></iframe>
				<h3>The DarkMOD</h3>
				<p>Many of the sound effects were created by Michael. They can be heard in the mix. Sounds such as "loot" sound, change tool sound, various dripping water sounds in the caverns, etc.</p>
			</div>
		</div>
	</div>

	<footer id="footer" class="clearfix">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 col-sm-6">
					<div id="copyright">Copyright &copy; 2000-<?php echo date("Y"); ?> myersmedia</div>
					<p>&ldquo;He who has ears, let him hear&rdquo; - <em>Matthew 11:15</em></p>
				</div>
				<div class="col-lg-6 col-sm-6 text-right">
					<ul class="footer-links">
						<li><a href="/">Home</a></li>
						<li><a href="about">About</a></li>
						<li><a href="music">Music</a></li>
						<li><a href="voiceover">Voiceover</a></li>
						<li><a href="sounddesign" class="active">Sound Design</a></li>
						<li><a href="fees">Arranging &amp; Copy</a></li>
						<li><a href="projects">Projects</a></li>
						<li><a href="studio">Studio</a></li>
						<li><a href="contact">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

<?php include 'inc/js.inc' ?>

</body>
</html>