<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'inc/header.inc' ?>
<meta name="description" content="Find out more about Michael R. Myers and his various talents." />
<meta name="author" content="Michael R. Myers">
<title>About | myersmedia - Saxophonist, composer, arranger, voiceover artist, and sound designer Michael R. Myers</title>
<?php include 'inc/css.inc' ?>

<style type="text/css">
.modal img {
	max-width:100%;
}
.modal-content {
	text-align:center;
}
</style>

</head>
<body class="about">

	<a href="#content" class="sr-only sr-only-focusable">Skip to main content</a>

	<nav class="navbar navbar-expand-lg navbar-inverse navbar-fixed-top bg-faded">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<a class="navbar-brand" href="/"><span>myers</span>media</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="nav-item"><a class="nav-link" href="/">Home</a></li>
					<li class="nav-item active"><a class="nav-link" href="about">About</a></li>
					<li class="nav-item"><a class="nav-link" href="music">Music</a></li>
					<li class="nav-item"><a class="nav-link" href="voiceover">Voiceover</a></li>
					<li class="nav-item"><a class="nav-link" href="sounddesign">Sound Design</a></li>
					<li class="nav-item"><a class="nav-link" href="fees">Arranging &amp; Copy</a></li>
					<li class="nav-item"><a class="nav-link" href="projects">Projects</a></li>
					<li class="nav-item"><a class="nav-link" href="studio">Studio</a></li>
					<li class="nav-item"><a class="nav-link" href="contact">Contact</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>


	<!-- Main jumbotron -->
	<div class="jumbotron">
		<div class="container">
			<div class="col-md-12">
				<h1>about</h1>
				<h2>More about Michael R. Myers</h2>
			</div><!--/col-md-12-->
		</div><!--/container-->
	</div><!--/jumbotron-->


<div class="container">
	<div class="col-lg-12">
		<div class="row">
			<div class="col-md-9">
				<h3>About Michael</h3>
				<p>Michael R. Myers <em>(multi-instrumentalist, composer, arranger, producer)</em> is a native of the western Appalachian mountains in North Carolina.</p>
				<p>His experience has led him down many different roads, having performed and worked on projects with many greats like: Chris Vadala (Chuck Mangione Quartet), Lenny Pickett (Saturday Night Live Band, Tower of Power), Ernie Watts (The Tonight Show, The Color Purple), Barbara McNair (The Barbara McNair Show), Ed Shaughnessy (The Tonight Show Band), Loonis McGlohan, Steve Hansgen (Minor Threat), Stephen Pollock (<a href="http://www.newcenturysax.com/" target="_blank">New Century Saxophone Quartet</a>), Tom Constanten (Grateful Dead), WLOS-13's Bob Caldwell and many others.</p>
				<p>He has also made numerous appearances performing and recording with different groups such as the <a href="http://www.lenoirsax.org" target="_blank">Lenoir Saxophone Ensemble</a>, the Nouveau Passé Orchestra, the <a href="http://www.wpsymphony.org/" target="_blank">Western Piedmont Symphony</a>, the <a href="http://www.blueridgeorchestra.org/" target="_blank">Blue Ridge Orchestra</a>, the <a href="http://www.ashevillesymphony.org/" target="_blank">Asheville Symphony</a>, the <a href="http://www.nelsonriddlemusic.com/" target="_blank">Nelson Riddle Orchestra</a>, the <a href="http://www.buddykband.com/" target="_blank">Buddy K Big Band</a>, <a href="http://www.thetemptations.net/main.html" target="_blank">The Temptations</a>, <a href="http://www.velvettruckstop.com/" target="_blank">Velvet Truckstop</a>, <a href="http://www.ashevillejazz.org/" target="_blank">The Asheville Jazz Orchestra</a> as well as many other combos and bands in the easern United States and performances in Europe.</p>
				<p>Now his music and other work can be found in projects for the Kennedy Center, the Red Cross, the DarkMOD, The Nameless MOD, The Beatles Rarity, Fab 4 Radio, and more.</p>
				<p>Michael has also been a music, recording, and technology educator for many years.</p>
				<h3>Endorsements</h2>
				<p>Michael is an official L&eacute;g&egrave;re Reeds artist and proudly endorses their reeds for all single-reed woodwinds. <a href="http://www.legere.com/artists/lenoir-sax">Visit the Lenoir Sax profile page at L&eacute;g&egrave;re.com for more details</a>.</p>
				<p><a href="http://www.legere.com/" target="_new"><img src="img/marketing/logo.legere.png" alt="L&eacute;g&egrave;re Reeds"></a></p>
				<!--
				<h3>Musical Influences</h3>
				<p>Percy Grainger, Bernard Herrmann, John Williams, Wendy Carlos, Danny Elfman, Elmer Bernstein, Michael Giacchino, Claude Debussy, Patrick Doyle, Jeremy Soule, Jerry Goldsmith, Nobuo Uematsu, Anne Dudley, The Art of Noise, The Buggles, John Barry, The Beatles, George Martin, Brian Wilson, Jeff Lynne and the Electric Light Orchestra, Thomas Dolby, Jellyfish (Andy Sturmer, Roger Manning Jr., and Jason Faulkner), Trent Reznor, Bill Brown, Rob Pottorf, Buddy Holly, Philip Glass, Richard Wagner, Wolfgang Amadeus Mozart, J.S. Bach, Jean Sibelius, Frederick Delius, Ralph Vaughn Williams, Sergei Prokofiev, Erik Satie, Robert Moog (for his great innovations and for being a really cool guy!), Beastie Boys, The Fat Man (George Alistair Sanger), David Bowie, Prince, XTC, Rufus Wainwright, Erich Korngold, Peter Schickele (and his humor via P.D.Q. Bach), Pugwash, Bruce Broughton, Cab Calloway, Raymond Scott, Monty Python, Basil Poledouris, Carl Stalling, Ben Folds, Moby, Soul Coughing, Frank Zappa, Jim Boggia, Alan Parsons, Bleu, and many, many more!</p>
				-->
			</div>
			<div class="col-md-3 text-center">
				<h4>Photos</h4>
				<ul class="picturelist">
					<li class="thumb"><a data-toggle="modal" data-target="#img01" href="/img/photos/lennypickett.jpg" title="With Lenny Pickett (of SNL) after a performance"><img src="/img/photos/lennypickett.thumb.jpg" alt="With Lenny Pickett (of SNL) after a performance" title="With Lenny Pickett (of SNL) after a performance"></a></li>
					<li class="thumb"><a data-toggle="modal" data-target="#img02" href="/img/photos/erniewatts.jpg" title="With Ernie Watts (The Tonight Show Band)"><img src="/img/photos/erniewatts.thumb.jpg" alt="With Ernie Watts (The Tonight Show Band)" title="With Ernie Watts (The Tonight Show Band)"></a></li>
					<li class="thumb"><a data-toggle="modal" data-target="#img03" href="/img/photos/chrisvadala.jpg" title="With Chris Vadala (Chuck Mangione Band)"><img src="/img/photos/chrisvadala.thumb.jpg" alt="With Chris Vadala (Chuck Mangione Band)" title="With Chris Vadala (Chuck Mangione Band)"></a></li>
					<li class="thumb"><a data-toggle="modal" data-target="#img04" href="/img/photos/lse_group.jpg" title="With Lenoir Sax"><img src="/img/photos/lse_group.thumb.jpg" alt="" title=""></a></li>
					<li class="thumb"><a data-toggle="modal" data-target="#img05" href="/img/photos/barbramcnair.jpg" title="A big band performance with Barbara McNair"><img src="/img/photos/barbramcnair.thumb.jpg" alt="A big band performance with Barbara McNair" title="A big band performance with Barbara McNair"></a></li>
					<li class="thumb"><a data-toggle="modal" data-target="#img06" href="/img/photos/kc_themichaels.jpg" title="The two Michaels at The Kennedy Center"><img src="/img/photos/kc_themichaels.thumb.jpg" alt="The two Michaels at The Kennedy Center" title="The two Michaels at The Kennedy Center"></a></li>
					<li class="thumb"><a data-toggle="modal" data-target="#img07" href="/img/photos/babysax.jpg" title=""><img src="/img/photos/babysax.thumb.jpg" alt="" title=""></a></li>
					<li class="thumb"><a data-toggle="modal" data-target="#img08" href="/img/photos/nriddle.jpg" title=""><img src="/img/photos/nriddle.thumb.jpg" alt="" title=""></a></li>
					<li class="thumb"><a data-toggle="modal" data-target="#img09" href="/img/photos/buddyrich.jpg" title=""><img src="/img/photos/buddyrich.thumb.jpg" alt="" title=""></a></li>
		     	    	</ul>
			</div><!-- /col-md-6 -->
		</div><!--/row-->
	</div><!--/col-lg-12-->
</div><!--/container-->


	<footer id="footer" class="clearfix">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 col-sm-6">
					<div id="copyright">Copyright &copy; 2000-<?php echo date("Y"); ?> myersmedia</div>
					<p>&ldquo;He who has ears, let him hear&rdquo; - <em>Matthew 11:15</em></p>
				</div>
				<div class="col-lg-6 col-sm-6 text-right">
					<ul class="footer-links">
						<li><a href="/">Home</a></li>
						<li><a href="about" class="active">About</a></li>
						<li><a href="music">Music</a></li>
						<li><a href="voiceover">Voiceover</a></li>
						<li><a href="sounddesign">Sound Design</a></li>
						<li><a href="fees">Arranging &amp; Copy</a></li>
						<li><a href="projects">Projects</a></li>
						<li><a href="studio">Studio</a></li>
						<li><a href="contact">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

<div class="modal fade" id="img01" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/lennypickett.jpg" alt="" title="">
				<p>Michael hanging out with Lennie Pickett after a masterclass.</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="img02" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/erniewatts.jpg" alt="" title="">
				<p>Michael hanging out with Ernie Watts backstage after SaxFest.</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="img03" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/chrisvadala.jpg" alt="" title="">
				<p>Michael with Chris Vadala after a masterclass.</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="img04" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/lse_group.jpg" alt="" title="">
				<p>Michael with other members of Lenoir Sax.</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="img05" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/barbramcnair.jpg" alt="" title="">
				<p>Michael with Barbra McNair after a concert.</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="img06" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/kc_themichaels.jpg" alt="" title="">
				<p>Michael and Michael at the Kennedy Center.</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="img07" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/babysax.jpg" alt="" title="">
				<p>Michael playing his favorite little toy.</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="img08" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/nriddle.jpg" alt="" title="">
				<p>Michael with the Nelson Riddle Orchestra.</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="img09" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/buddyrich.jpg" alt="" title="">
				<p>Michael playing on Rick Dilling's Buddy Rich Tribute concert - September 2017</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->



<?php include 'inc/js.inc' ?>

</body>
</html>