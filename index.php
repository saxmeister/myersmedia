<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'inc/header.inc' ?>
<meta name="description" content="Home of saxophonist, composer, arranger, voiceover artist, and sound designer Michael R. Myers. Original music and sound design for film, stage, and video games." />
<title>myersmedia - Saxophonist, composer, arranger, voiceover artist, and sound designer Michael R. Myers</title>
<?php include 'inc/css.inc' ?>
<link rel="stylesheet" media="screen" href="css/jumbotron.css">
<style type="text/css">
#footer {margin-top:0}
</style>
</head>
<body class="home">

	<a href="#content" class="sr-only sr-only-focusable">Skip to main content</a>

	<nav class="navbar navbar-expand-lg navbar-inverse navbar-fixed-top bg-faded">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<a class="navbar-brand" href="/"><span>myers</span>media</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="nav-item active"><a class="nav-link" href="/">Home</a></li>
					<li class="nav-item"><a class="nav-link" href="about">About</a></li>
					<li class="nav-item"><a class="nav-link" href="music">Music</a></li>
					<li class="nav-item"><a class="nav-link" href="voiceover">Voiceover</a></li>
					<li class="nav-item"><a class="nav-link" href="sounddesign">Sound Design</a></li>
					<li class="nav-item"><a class="nav-link" href="fees">Arranging &amp; Copy</a></li>
					<li class="nav-item"><a class="nav-link" href="projects">Projects</a></li>
					<li class="nav-item"><a class="nav-link" href="studio">Studio</a></li>
					<li class="nav-item"><a class="nav-link" href="contact">Contact</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>

	<div class="fluid-container" id="content" tabindex="-1">

		<!-- Main jumbotron -->
		<div class="jumbotron">
			<div class="container-fluid" style="width:100%;height:100%;background-color:rgba(0,0,0,0.3);">
				<div class="container">
					<h1><em style="color:#337AB7">myers</em>media</h1>
					<h2>Home of Michael R. Myers - saxophonist, composer, arranger, voiceover artist, sound designer, and web developer</h2>
					<p>This multi-instrumentalist and all-around sound tech guy can give your project just what it needs, from a catchy jingle, to voiceover work, to full production services, to sound design.</p>
					<p><a href="about.php" class="btn btn-primary btn-lg" role="button">Learn more &raquo;</a></p>
				</div>
			</div>
		</div>

		<!-- Three columns of text below the carousel -->
		<div class="container marketing">
			<div class="row">

				<div class="col-lg-4">
					<div class="img-circle music"></div>
					<h2>Composition/Arranging</h2>
					<p>Michael's compositions and arrangements can be heard in commercial promos, video games, on the radio, and on the stage.</p>
					<p><a href="music" class="btn btn-outline-primary" role="button">Find out more &raquo;</a></p>
				</div><!-- /.col-lg-4 -->

				<div class="col-lg-4">
					<div class="img-circle voiceover"></div>
					<h2>Voiceover</h2>
					<p>From commercials to video games to sports arenas, Michael's voice continues to makes its presence known while setting the right mood.</p>
					<p><a href="voiceover" class="btn btn-outline-primary" role="button">Listen now &raquo;</a></p>
				</div><!-- /.col-lg-4 -->

				<div class="col-lg-4">
					<div class="img-circle sound-design"></div>
					<h2>Sound Design</h2>
					<p>From beeps to bombastic brass, Michael's sound designs can bring any environment to life, whether stage, video, or virtual environments.</p>
					<p><a href="sounddesign" class="btn btn-outline-primary" role="button">Hear examples &raquo;</a></p>
				</div><!-- /.col-lg-4 -->

			</div><!-- /.row -->
		</div>



		<section class="featurette-section" style="background-image:url('/img/marketing/workplace-1920.jpg')">
			<div class="container">
				<div class="row featurette">
					<div class="col-md-9">
						<h3 class="featurette-heading">It doesn't stop there! <span class="text-muted">Look what we can do&hellip;</span></h3>
						<p class="lead">Not only does myersmedia offer full services for all of your sound needs, they can also offer full service web design. Simple, classic, elegant, or something completely out there; we can make it happen. We maintain 508 compliance and fully responsive or adaptive designs to allow your sites to be viewed on any device.</p>
					</div>
					<div class="col-md-3">
						<div class="img-circle boom-box"></div>
					</div>
				</div>
			</div>
		</section>

		<section class="featurette-section" style="background-image:url('/img/marketing/milky-way-1920.jpg')">
			<div class="container">
				<div class="row featurette">
					<div class="col-md-3">
						<div class="img-circle headphone"></div>
					</div>
					<div class="col-md-9">
						<h3 class="featurette-heading">Need a score? <span class="text-muted">You've got it!</span></h3>
						<p class="lead">myersmedia also offers electronic copying and scoring services to allow your handwritten or MIDI files to be rendered as professional scores. Whether you need some arranging or not, we can provide full score editing and printing services for all your needs.</p>
						<a class="btn btn-outline-primary" href="/fees" role="button">Get more info</a>
					</div>
				</div>
			</div>
		</section>

		<!--
		<section class="featurette-section" style="background-image:url('/img/marketing/jelly-fish-1920.jpg')">
			<div class="container">
				<div class="row featurette">
					<div class="col-md-9">
						<h3 class="featurette-heading">But wait, there's more! <span class="text-muted">Recording!</span></h3>
						<p class="lead">myersmedia also offers studio services for voiceovers and smaller ensembles. We can even do smaller scale location recordings or partner with one of our partner studios for larger projects.</p>
						<a class="btn btn-outline-primary" href="/studio" role="button">Get more info</a>
					</div>
					<div class="col-md-3">
						<div class="img-circle mic"></div>
					</div>
				</div>
			</div>
		</section>
		-->
		<!-- /END THE FEATURETTES -->

		<!-- Recent projects rotator -->
	
	</div><!--#/container-fluid-->

	<footer id="footer" class="clearfix">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 col-sm-6">
					<div id="copyright">Copyright &copy; 2000-<?php echo date("Y"); ?> myersmedia</div>
					<p>&ldquo;He who has ears, let him hear&rdquo; - <em>Matthew 11:15</em></p>
				</div>
				<div class="col-lg-6 col-sm-6 text-right">
					<ul class="footer-links">
						<li><a href="/" class="active">Home</a></li>
						<li><a href="about">About</a></li>
						<li><a href="music">Music</a></li>
						<li><a href="voiceover">Voiceover</a></li>
						<li><a href="sounddesign">Sound Design</a></li>
						<li><a href="fees">Arranging &amp; Copy</a></li>
						<li><a href="projects">Projects</a></li>
						<li><a href="studio">Studio</a></li>
						<li><a href="contact">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

<?php include 'inc/js.inc' ?>

</body>
</html>