<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'inc/header.inc' ?>
<meta name="description" content="Find out more about Michael R. Myers and his various talents." />
<meta name="author" content="Michael R. Myers">
<title>Arranging &amp; Copy Fees | myersmedia - Saxophonist, composer, arranger, voiceover artist, and sound designer Michael R. Myers</title>
<?php include 'inc/css.inc' ?>

</head>

<body class="fees">

	<a href="#content" class="sr-only sr-only-focusable">Skip to main content</a>

	<nav class="navbar navbar-expand-lg navbar-inverse navbar-fixed-top bg-faded">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<a class="navbar-brand" href="/"><span>myers</span>media</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="nav-item"><a class="nav-link" href="/">Home</a></li>
					<li class="nav-item"><a class="nav-link" href="about">About</a></li>
					<li class="nav-item"><a class="nav-link" href="music">Music</a></li>
					<li class="nav-item"><a class="nav-link" href="voiceover">Voiceover</a></li>
					<li class="nav-item"><a class="nav-link" href="sounddesign">Sound Design</a></li>
					<li class="nav-item active"><a class="nav-link" href="fees">Arranging &amp; Copy</a></li>
					<li class="nav-item"><a class="nav-link" href="projects">Projects</a></li>
					<li class="nav-item"><a class="nav-link" href="studio">Studio</a></li>
					<li class="nav-item"><a class="nav-link" href="contact">Contact</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>

	<!-- Main jumbotron -->
	<div class="jumbotron">
		<div class="container">
			<div class="col-md-12">
				<h1>copyist/arranging</h1>
				<h2>Michael's info and fees for copying/arranging</h2>
			</div><!--/col-md-12-->
		</div><!--/container-->
	</div><!--/jumbotron-->

<div class="container">

	<div class="col-lg-12">

		<div class="row">

			<div class="col-md-12">

				<h3>Copyist/Arranging Work</h3>

				<p>Need parts copied or transposed for your latest performance or project? Want some comp tracks to be fleshed out as full horn or string sections? It can be accomplished!</p>

				<p>Standard pricing is <strong>$25/hour</strong>. This includes the following:</p>

				<ul>

					<li>Up to five part arranging without additional instruction</li>

					<li>Complex scoring</li>

					<li>Scoring of parts produced by someone other than myself</li>

				</ul>

				<h3>Standard Copy Pricing</h3>

				<p>For typical projects, pricing is calculated from the lower of either $25/hour or a set per-page rate.</p>

				<h4>Per-Page Copy Pricing Rates</h4>

				<ul>

					<li>Full-Score Page - <strong>$25.00/page</strong></li>

					<li>Small Ensemble (Duos, Trios, Quartets, Quintets) - <strong>$15.00/page</strong></li>

					<li>Solo Instrument with Piano Accompaniment - <strong>$15.00/page</strong></li>

					<li>Solo Piano - <strong>$15.00/page</strong></li>

					<li>Single-Line parts - <strong>$12.50/page</strong></li>

					<li>Single-line Parts extracted from a completed score - <strong>$8.00/page</strong></li>

				</ul>

				<p>*Note: Per-page rates are based on the finished project, not on the original scores submitted.</p>

				<h4>Additional Version Fees</h4>

				<p>The following fees apply to projects that include multiple versions of the same piece of music (such as having a piece transposed to three different keys).  The percentage listed represents the increase to the maximum per-page pricing and does not affect the per-hour pricing at all.</p>

				<ul>

					<li>Additional transposition - <strong>10% of total project</strong></li>

					<li>Additional instrumentation - <strong>20% of total project</strong></li>

					<li>Additional version with extensive differences - <strong>50% of total project</strong></li>

				</ul>

				<h4>Arranging Fees</h4>

				<p>For arranging, I request that any piano parts or other scores be marked up in a 3, 4, 5, or more part harmony and each line be specificed by instrumentation. Any time that this is left up to me charges must be hourly.</p>

				<ul>

					<li>Full-Score Page - <strong>$50.00/page</strong></li>

					<li>Small Ensemble (Duos, Trios, Quartets, Quintets) - <strong>$30.00/page</strong></li>

					<li>Solo Instrument with Piano Accompaniment - <strong>$30.00/page</strong></li>

				</ul>



				<h3>Additional Fees</h3>

				<p>Add 25% to the total per-page price if the score has to be created in any other version of Finale besides Finale 25.</p>

				<p>All corrections are complete free-of-charge.</p>



				<h3>Delivery</h3>

				<p>By default, all projects are delivered in PDF format via electronic transfer (email, FTP, Google Drive, etc.). I can only print 8.5" x 11" prints with a laser printer, so large format prints will need to be taken to your local document shop (such as Kinko's or OfficeMax) to print. Scores are not taped together and are delivered in loose-leaf format. All pages will be marked accordingly to prevent confusion.</p>



				<h3>Payment</h3>

				<p>I accept cash, cashier's checks, personal checks, and PayPal, due at completion.</p>

			</div>

		</div><!--/row-->

	</div><!--/col-lg-12-->

</div><!--/container-->





	<footer id="footer" class="clearfix">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 col-sm-6">
					<div id="copyright">Copyright &copy; 2000-<?php echo date("Y"); ?> myersmedia</div>
					<p>&ldquo;He who has ears, let him hear&rdquo; - <em>Matthew 11:15</em></p>
				</div>
				<div class="col-lg-6 col-sm-6 text-right">
					<ul class="footer-links">
						<li><a href="/">Home</a></li>
						<li><a href="about" class="active">About</a></li>
						<li><a href="music">Music</a></li>
						<li><a href="voiceover">Voiceover</a></li>
						<li><a href="sounddesign">Sound Design</a></li>
						<li><a href="fees">Arranging &amp; Copy</a></li>
						<li><a href="projects">Projects</a></li>
						<li><a href="studio">Studio</a></li>
						<li><a href="contact">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

<div class="modal fade" id="img01" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/lennypickett.jpg" alt="" title="">
				<p>Michael hanging out with Lennie Pickett after a masterclass.</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="img02" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/erniewatts.jpg" alt="" title="">
				<p>Michael hanging out with Ernie Watts backstage after SaxFest.</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="img03" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/chrisvadala.jpg" alt="" title="">
				<p>Michael with Chris Vadala after a masterclass.</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="img04" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/lse_group.jpg" alt="" title="">
				<p>Michael with other members of Lenoir Sax.</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="img05" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/barbramcnair.jpg" alt="" title="">
				<p>Michael with Barbra McNair after a concert.</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="img06" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/kc_themichaels.jpg" alt="" title="">
				<p>Michael and Michael at the Kennedy Center.</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="img07" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>

			<div class="modal-body">

				<img src="/img/photos/babysax.jpg" alt="" title="">

				<p>Michael playing his favorite little toy.</p>

			</div>

		</div><!-- /.modal-content -->

	</div><!-- /.modal-dialog -->

</div><!-- /.modal -->



<div class="modal fade" id="img08" tabindex="-1" role="dialog">

	<div class="modal-dialog" role="document">

		<div class="modal-content">

			<div class="modal-header">

				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

			</div>

			<div class="modal-body">

				<img src="/img/photos/nriddle.jpg" alt="" title="">

				<p>Michael with the Nelson Riddle Orchestra.</p>

			</div>

		</div><!-- /.modal-content -->

	</div><!-- /.modal-dialog -->

</div><!-- /.modal -->



<div class="modal fade" id="img09" tabindex="-1" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
				<img src="/img/photos/buddyrich.jpg" alt="" title="">
				<p>Michael playing on Rick Dilling's Buddy Rich Tribute concert - September 2017</p>
			</div>
		</div><!-- /.modal-content -->
	</div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<?php include 'inc/js.inc' ?>

</body>
</html>