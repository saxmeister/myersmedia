<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="keywords" content="score, film, video, games, voiceover, design, sound, web, movie, music, original, composition, independent" />
<meta name="description" content="Various examples of Michael's work as a voiceover artist." />
<meta name="author" content="Michael R. Myers">

<link rel="icon" href="favicon.ico">

<title>Voiceover | myersmedia - Saxophonist, composer, arranger, voiceover artist, and sound designer Michael R. Myers</title>

<?php include 'inc/css.inc' ?>

<style type="text/css" media="screen">
.imgbox a {
	display:block;
	text-align:center;
}

.imgbox img {
	height:75px;
	margin:20px auto !important;
}
</style>

</head>

<body class="voiceover">

	<a href="#content" class="sr-only sr-only-focusable">Skip to main content</a>

	<nav class="navbar navbar-expand-lg navbar-inverse navbar-fixed-top bg-faded">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbar" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<a class="navbar-brand" href="/"><span>myers</span>media</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="nav-item"><a class="nav-link" href="/">Home</a></li>
					<li class="nav-item"><a class="nav-link" href="about">About</a></li>
					<li class="nav-item"><a class="nav-link" href="music">Music</a></li>
					<li class="nav-item active"><a class="nav-link" href="voiceover">Voiceover</a></li>
					<li class="nav-item"><a class="nav-link" href="sounddesign">Sound Design</a></li>
					<li class="nav-item"><a class="nav-link" href="fees">Arranging &amp; Copy</a></li>
					<li class="nav-item"><a class="nav-link" href="projects">Projects</a></li>
					<li class="nav-item"><a class="nav-link" href="studio">Studio</a></li>
					<li class="nav-item"><a class="nav-link" href="contact">Contact</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>


	<!-- Main jumbotron -->
	<div class="jumbotron" style="background-image:url('img/jumbotron/mics_blur.jpg');">
		<div class="container">
			<div class="col-md-12">
				<h1>voiceover</h1>
				<h2>Below are some highlights from Michael's voiceover work</h2>
			</div>
		</div>
	</div>


	<div class="container">
		<h2>Games</h2>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<iframe width="100%" height="300" src="https://www.youtube.com/embed/5sJNodRD6EI" frameborder="0" allowfullscreen></iframe>
				<h3>The Nameless MOD: King Kashue</h3>
				<p>A walkthrough parts of The Nameless MOD featuring one of the characters Michael voiced for this project: King Kashue.<br /><a class="btn btn-default btn-primary" href="http://thenamelessmod.com/real/" target="_new">Download The Nameless MOD</a></p>
			</div>
			
			<div class="col-md-6">
				<iframe width="100%" height="300" src="https://www.youtube.com/embed/dGjfyvrYPZA" frameborder="0" allowfullscreen></iframe>
				<h3>Thief</h3>
				<p>Voiceover work Michael performed for a fan mission. This is the briefing from "On The Trail Of A Fence."<br /><a class="btn btn-default btn-primary" href="http://forums.thedarkmod.com/topic/14868-tds-fan-mission-on-the-trail-of-a-fence-by-beleg-cuthalion-070613/" target="_new">Download &quot;On The Trail Of A Fence&quot; now</a></p>
				<p>Quotes on the forums about Michael's performance:</p>
				<p><i>&quot;Probably the best Garrett impersonation yet.&quot;</i> - Theothesnopp, The DarkMOD forums</p>
				<p><i>&quot;And the voice of saxmeister is just.....Garrett voice!&quot;</i> - lowenz, The MarkMOD forums</p>
				<p><i>&quot;&hellip;yes, saxmeister's job is downright awesome.&quot;</i> - Beleg Cúthalion, author of "On The Trail Of A Fence"</p>
			</div>
		</div><!--/row-->
	</div><!--/container-->

	<footer id="footer" class="clearfix">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 col-sm-6">
					<div id="copyright">Copyright &copy; 2000-<?php echo date("Y"); ?> myersmedia</div>
					<p>&ldquo;He who has ears, let him hear&rdquo; - <em>Matthew 11:15</em></p>
				</div>
				<div class="col-lg-6 col-sm-6 text-right">
					<ul class="footer-links">
						<li><a href="/">Home</a></li>
						<li><a href="about">About</a></li>
						<li><a href="music">Music</a></li>
						<li><a href="voiceover" class="active">Voiceover</a></li>
						<li><a href="sounddesign">Sound Design</a></li>
						<li><a href="fees">Arranging &amp; Copy</a></li>
						<li><a href="projects">Projects</a></li>
						<li><a href="studio">Studio</a></li>
						<li><a href="contact">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

<?php include 'inc/js.inc' ?>

</body>
</html>