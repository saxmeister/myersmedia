<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<meta name="keywords" content="score, film, video, games, voiceover, design, sound, web, movie, music, original, composition, independent" />
<meta name="description" content="Various projects Michael has worked on over the past several years." />
<meta name="author" content="Michael R. Myers">

<link rel="icon" href="favicon.ico">

<title>Projects | myersmedia - Saxophonist, composer, arranger, voiceover artist, and sound designer Michael R. Myers</title>

<?php include 'inc/css.inc' ?>

<style type="text/css">
.jumbotron {margin-bottom:0}
#footer {margin-top:0}

.row [class*="col-"] + [class*="col-"] {
    margin-bottom: 80px;
}

@media (min-width: 1200px) {
    .row [class*="col-lg-"] + [class*="col-lg-"] {
        margin-top: 0;
    }
}
@media (min-width: 992px) {
    .row [class*="col-md-"] + [class*="col-md-"] {
        margin-top: 0;
    }
}
@media (min-width: 768px) {
    .row [class*="col-sm-"] + [class*="col-sm-"] {
        margin-top: 0;
    }
}
</style>
</head>

<body class="projects">

	<a href="#content" class="sr-only sr-only-focusable">Skip to main content</a>

	<nav class="navbar navbar-expand-lg navbar-inverse navbar-fixed-top bg-faded">
		<div class="container">
			<div class="navbar-header">
				<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbar" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<a class="navbar-brand" href="/"><span>myers</span>media</a>
			</div>
			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav mr-auto">
					<li class="nav-item"><a class="nav-link" href="/">Home</a></li>
					<li class="nav-item"><a class="nav-link" href="about">About</a></li>
					<li class="nav-item"><a class="nav-link" href="music">Music</a></li>
					<li class="nav-item"><a class="nav-link" href="voiceover">Voiceover</a></li>
					<li class="nav-item"><a class="nav-link" href="sounddesign">Sound Design</a></li>
					<li class="nav-item"><a class="nav-link" href="fees">Arranging &amp; Copy</a></li>
					<li class="nav-item active"><a class="nav-link" href="projects">Projects</a></li>
					<li class="nav-item"><a class="nav-link" href="studio">Studio</a></li>
					<li class="nav-item"><a class="nav-link" href="contact">Contact</a></li>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>


	<!-- Main jumbotron -->
	<div class="jumbotron">
		<div class="container">
			<div class="col-md-12">
				<h1>projects</h1>
				<h2>Below are some work highlights from several projects</h2>
			</div>
		</div>
	</div>

	<div class="fluid-container">

		<section class="featurette-section" style="background-image:url('/img/marketing/artsedge-1920.jpg')">
			<div class="container">
				<div class="row featurette">
					<div class="col-md-7">
						<h2>ARTSEDGE</h2>
						<p>Michael worked with the educational branch of the Kennedy Center, called ARTSEDGE, which specializes is bringing music into the educational world. He assisted on the "Perfect Pitch" project which utilized Michael's original compositions in several differing styles. Having a great love of teaching and arts education Michael was honored to assist in any way possible for this group's purpose.</p>
						<div><a class="btn btn-default btn-outline-info text-center" target="_blank" href="http://artsedge.kennedy-center.org/perfectpitch/">ARTSEDGE Perfect Pitch</a></div>
					</div>
				</div>
			</div>
		</section>

		<section class="featurette-section" style="background-image:url('/img/marketing/nameless-1920.jpg')">
			<div class="container">
				<div class="row featurette">
					<div class="col-md-7">
						<h2>The Nameless MOD</h2>
						<p>After being in development for many years, The Nameless MOD has been released. Michael always enjoyed the <em>Deus Ex</em> games for their complex story lines and was happy to be involved with this project. The development team created a deep environment with a great storyline that will not disappoint. You can hear Michael voicing two separate roles: King Kashue and Shadowcode.</p>
						<div><a class="btn btn-outline-info text-center" target="_blank" href="http://thenamelessmod.com/">The Nameless MOD</a></div>
					</div>
				</div>
			</div>
		</section>

		<section class="featurette-section" style="background-image:url('/img/marketing/darkmod-1920.jpg')">
			<div class="container">
				<div class="row featurette">
					<div class="col-md-7">
						<h2>The DarkMOD</h2>
						<p>Being a huge fan of the <em>Thief</em> series of games, Michael discovered this MOD for the DOOM3 engine set within a <em>Thief</em>-like universe. He simply <em>had</em> to become involved and began sound design as well as ambient music for this project. The whole MOD team is incredibly talented and will really shine against many of the other MODs out there. And now it's even a standalone installation.</p>
						<div><a class="btn btn-outline-info text-center" target="_blank" href="http://www.thedarkmod.com/main/">The DarkMOD</a></div>
					</div>
				</div>
			</div>
		</section>

		<section class="featurette-section" style="background-image:url('/img/marketing/redcross-1920.jpg')">
			<div class="container">
				<div class="row featurette">
					<div class="col-md-7">
						<h2>Red Cross</h2>
						<p>Michael teamed up with <a href="http://www.oddkid.com/" target="_blank">Oddkid Productions</a> supplying on location sound recording, clean up and mastering as well as composition of the music for one of the Red Cross' diversity training videos to be supplied to the American Red Cross' Southeast regional offices.</p>
						<div><a class="btn btn-outline-info text-center" target="_blank" href="http://www.oddkid.com/">Oddkid Productions</a></div>
					</div>
				</div>
			</div>
		</section>

		<section class="featurette-section" style="background-image:url('/img/marketing/coverville-1920.jpg')">
			<div class="container">
				<div class="row featurette">
					<div class="col-md-7">
						<h2>Coverville</h2>
						<p>As a fan of the Coverville podcast, Michael responded to the call of host Brian Ibbott who sent out a search request for covers of Squeeze tracks back in 2008. Michael couldn't resist and completed a cover of Squeeze's "Take Me I'm Yours" to be included on the compilation.</p>
						<div><a class="btn btn-outline-info text-center" target="_blank" href="http://coverville.com/">Coverville</a></div>
					</div>
				</div>
			</div>
		</section>

		<section class="featurette-section" style="background-image:url('/img/marketing/schlockwerk-1920.jpg')">
			<div class="container">
				<div class="row featurette">
					<div class="col-md-7">
						<h2>Schl&ouml;ckwerk</h2>
						<p><i>Schl&ouml;ckwerk</i> is Michael's alter-ego that allows him freedom to pursue projects he normally would not pursue. This project band is a lovingly crafted parody of all of the things that Michael loved growing up - a pastiche respectfully digging at his favorite guilty pleasures.</p>
						<div><a class="btn btn-outline-info text-center" target="_blank" href="https://soundcloud.com/user-799391145">Schl&ouml;ckwerk on SoundCloud</a></div>
					</div>
				</div>
			</div>
		</div>

	</div>

	<footer id="footer" class="clearfix">
		<div class="container-fluid">
			<div class="row">
				<div class="col-lg-6 col-sm-6">
					<div id="copyright">Copyright &copy; 2000-<?php echo date("Y"); ?> myersmedia</div>
					<p>&ldquo;He who has ears, let him hear&rdquo; - <em>Matthew 11:15</em></p>
				</div>
				<div class="col-lg-6 col-sm-6 text-right">
					<ul class="footer-links">
						<li><a href="/">Home</a></li>
						<li><a href="about">About</a></li>
						<li><a href="music">Music</a></li>
						<li><a href="voiceover">Voiceover</a></li>
						<li><a href="sounddesign">Sound Design</a></li>
						<li><a href="fees">Arranging &amp; Copy</a></li>
						<li><a href="projects" class="active">Projects</a></li>
						<li><a href="studio">Studio</a></li>
						<li><a href="contact">Contact</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>

<?php include 'inc/js.inc' ?>

</body>
</html>